# PKI

[English version](README-en.md)

## Description

De simples scripts bash pour gérer PKI avec openssl.

Il s'agit de scripts de tests pour manipuler openssl
et comprendre les mécanismes de gestion d'une PKI.

## Attention

La gestion des secrets n'est pas faite convenablement
pour un usage en production, à moins que ce soit pour
générer des environnements de tests ou de qualification.

## Quickstart

```bash
    # Ajouter la completion à votre environnement
    . pki-completion.sh

    # Initialiser la pki
    pki init

    # Créer une requête de certificat pour le server myhost.local
    pki request new myhost.local

    # Vérifier la liste des certicat en attente de signature
    pki requests

    # Signer le certificat en attente pour myhost.local
    pki cert sign myhost.local

    # Afficher la liste des certificats de la pki
    pki certs

    # Vérifier l'état d'un certificat en donnat le chemin du fichier
    pki cert status ./myCA/certs/myhost.local.pem

    # Ou en utiisant le numéro de série.
    pki cert status 1000

    # Pour avoir une vision globale
    pki status

```

Plus d'aide

```bash
    pki help <TAB> <TAB>
```

## Automatisation pour des tests

```bash
   export BATCHMODE=yes
   export C="FR"
   export ST="Bourgogne"
   export L="Dijon"
   export O="Ma petit entreprise de Mayonnaise"
   export OU="IT de $O"
   export CN="CA de $O"

   hosts=( host1.local host2.local host3.local)
   pki init
   for h in "${hosts[@]}"; do
       pki request new "$h"
       pki cert sign "$h"
   done
   pki status
```

## Conteneur

Build

```bash
     docker build -t . pki
```

Usage simple créé myCA dans le dossier courant

```bash
    docker run -ti --rm  -v.:/pki pki init
```

Pour passer les paramètres de configuration de la pki, utiliser l'option
`-e | --env` de docker ou podman.

```bash
    docker run -ti --rm  -v.:/pki \
       --env CA_DIR=/pki/mayoCA \
       --env BATCHMODE=yes \
       --env C="FR" \
       --env ST="Bourgogne" \
       --env L="Dijon" \
       --env O="Ma petit entreprise de Mayonnaise" \
       --env OU="IT de Ma petit entreprise de Mayonnaise" \
       --env CN="CA de Ma petit entreprise de Mayonnaise" \
	   pki init
```

